module bookstore_items-api

go 1.18

require (
	github.com/elastic/go-elasticsearch/v8 v8.3.0
	github.com/gorilla/mux v1.8.0
	gitlab.com/guyamuff/bookstore_oauth-go v0.0.0-20220818160058-9f1d9fbbcf4d
)

require (
	github.com/elastic/elastic-transport-go/v8 v8.0.0-20211216131617-bbee439d559c // indirect
	github.com/mercadolibre/golang-restclient v0.0.0-20170701022150-51958130a0a0 // indirect
	go.uber.org/atomic v1.7.0 // indirect
	go.uber.org/multierr v1.6.0 // indirect
	go.uber.org/zap v1.21.0 // indirect
)
