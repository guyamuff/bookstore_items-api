package app

import (
	"bookstore_items-api/clients/elasticsearch"
	"bookstore_items-api/controllers"
	"github.com/gorilla/mux"
	"net/http"
)

func StartApp() {
	elasticsearch.Init()

	r := mux.NewRouter()
	r.HandleFunc("/ping", controllers.PingController.Ping)
	r.HandleFunc("/item/{id:[0-9]+}", controllers.ItemController.Get).Methods(http.MethodGet)
	r.HandleFunc("/item", controllers.ItemController.Create).Methods(http.MethodPost)

	srv := &http.Server{
		Addr:    "localhost:8082",
		Handler: r,
	}
	startErr := srv.ListenAndServe()
	if startErr != nil {
		panic(startErr)
	}
}
