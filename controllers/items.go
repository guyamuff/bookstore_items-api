package controllers

import (
	"bookstore_items-api/domains/items"
	"bookstore_items-api/services"
	"encoding/json"
	"github.com/gorilla/mux"
	"gitlab.com/guyamuff/bookstore_oauth-go/src/errors"
	"gitlab.com/guyamuff/bookstore_oauth-go/src/oauth"
	"io/ioutil"
	"net/http"
	"strconv"
)

type itemController interface {
	Get(http.ResponseWriter, *http.Request)
	Create(http.ResponseWriter, *http.Request)
}

type itemControllerImpl struct {
}

func newItemController() itemController {
	return &itemControllerImpl{}
}

var ItemController = newItemController()

func (c *itemControllerImpl) Get(w http.ResponseWriter, r *http.Request) {
	o := oauth.NewOAuth()
	_, err := o.GetAuth(r)
	if err != nil {
		errorResponse(err, w)
		return
	}

	params := mux.Vars(r)
	itemStr := params["id"]
	itemId, parseErr := strconv.ParseInt(itemStr, 10, 64)
	if parseErr != nil {
		restErr := errors.NewBadRequestError("Can't parse userId " + itemStr)
		errorResponse(restErr, w)
		return
	}
	item, restErr := services.ItemsService.Get(itemId)
	if restErr != nil {
		errorResponse(restErr, w)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	e := json.NewEncoder(w).Encode(item)
	if e != nil {
		panic(e)
	}
}

func (c *itemControllerImpl) Create(w http.ResponseWriter, r *http.Request) {
	var item items.Item
	b, err := ioutil.ReadAll(r.Body)
	if err != nil {
		restErr := errors.NewInternalError("Problem reading request body")
		errorResponse(restErr, w)
	}
	defer func() {
		e := r.Body.Close()
		if e != nil {
			panic(e)
		}
	}()

	err = json.Unmarshal(b, &item)
	if err != nil {
		restErr := errors.NewBadRequestError("Problem unmarshalling request body")
		errorResponse(restErr, w)
		return
	}

	o := oauth.NewOAuth()
	oa, restErr := o.GetAuth(r)
	if restErr != nil {
		errorResponse(restErr, w)
		return
	}
	item.Seller = oa.UserId

	newItem, restErr := services.ItemsService.Create(item)
	if restErr != nil {
		errorResponse(restErr, w)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)
	e := json.NewEncoder(w).Encode(newItem)
	if e != nil {
		panic(e)
	}
}

func errorResponse(restErr *errors.RestErr, w http.ResponseWriter) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(restErr.Status)
	e := json.NewEncoder(w).Encode(restErr)
	if e != nil {
		panic(e)
	}
}
