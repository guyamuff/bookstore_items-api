package controllers

import "net/http"

type pingController interface {
	Ping(http.ResponseWriter, *http.Request)
}

type pingControllerImpl struct {
}

var PingController = pingControllerImpl{}

func newPingController() pingController {
	return &pingControllerImpl{}
}

func (c *pingControllerImpl) Ping(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("pong\n"))
}
