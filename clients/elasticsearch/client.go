package elasticsearch

import (
	"bufio"
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/elastic/go-elasticsearch/v8"
	"gitlab.com/guyamuff/bookstore_oauth-go/src/logger"
	"io"
	"net/http"
	"time"
)

type esClientInterface interface {
	Index(string, any) (string, error)
}

type esClient struct {
	client *elasticsearch.Client
}

var Client esClientInterface

func Init() {
	cfg := elasticsearch.Config{
		Addresses: []string{
			"https://localhost:9200",
		},
		Username:               "elastic",
		Password:               "V-eYc3=*C-g5XsjoiJC-",
		CertificateFingerprint: "907b5189d3df5977dc5c93f2a477a9d14f81bf61d31a0aa80610669b068a36a8",
		EnableDebugLogger:      false,
		Logger: &ESLogger{
			requestBodyEnabled:  false,
			responseBodyEnabled: false,
		},
	}

	c, err := elasticsearch.NewClient(cfg)
	if err != nil {
		panic(err)
	}
	Client = &esClient{client: c}
}

func (c *esClient) Index(indexName string, body any) (string, error) {
	m, err := json.Marshal(body)
	if err != nil {
		return "", err
	}
	r := bytes.NewReader(m)
	resp, err := c.client.Index(indexName, r)
	if err != nil {
		return "", err
	}

	var bd = EsResp{}
	err = json.NewDecoder(resp.Body).Decode(&bd)
	if err != nil {
		return "", err
	}
	defer func() {
		err = resp.Body.Close()
		if err != nil {
			panic(err)
		}
	}()

	return bd.Id, nil
}

type EsResp struct {
	Id string `json:"_id"`
}

type ESLogger struct {
	requestBodyEnabled  bool
	responseBodyEnabled bool
}

func (l *ESLogger) LogRoundTrip(req *http.Request, resp *http.Response, err error, t time.Time, d time.Duration) error {

	logger.Log.Info(fmt.Sprintf("%s %s %s [status:%d request:%s]",
		t.Format(time.RFC3339),
		req.Method,
		req.URL.String(),
		resp.StatusCode,
		d.Truncate(time.Millisecond),
	))

	if l.RequestBodyEnabled() && req != nil && req.Body != nil && req.Body != http.NoBody {
		var buf bytes.Buffer
		if req.GetBody != nil {
			b, _ := req.GetBody()
			buf.ReadFrom(b)
		} else {
			buf.ReadFrom(req.Body)
		}
		logBodyAsText(&buf, ">")
	}
	if l.ResponseBodyEnabled() && resp != nil && resp.Body != nil && resp.Body != http.NoBody {
		defer resp.Body.Close()
		var buf bytes.Buffer
		buf.ReadFrom(resp.Body)
		logBodyAsText(&buf, "<")
	}
	if err != nil {
		logger.Log.Error("! ERROR: %v\n" + err.Error())
	}
	return nil
}

func (l *ESLogger) RequestBodyEnabled() bool {
	return l.requestBodyEnabled
}

func (l *ESLogger) ResponseBodyEnabled() bool {
	return l.responseBodyEnabled
}

func logBodyAsText(body io.Reader, prefix string) {
	scanner := bufio.NewScanner(body)
	for scanner.Scan() {
		s := scanner.Text()
		if s != "" {
			logger.Log.Info(fmt.Sprintf("%s %s", prefix, s))
		}
	}
}
