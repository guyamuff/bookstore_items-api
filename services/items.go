package services

import (
	"bookstore_items-api/domains/items"
	"gitlab.com/guyamuff/bookstore_oauth-go/src/errors"
)

type itemService interface {
	Get(int64) (*items.Item, *errors.RestErr)
	Create(items.Item) (*items.Item, *errors.RestErr)
}

type itemServiceImpl struct {
}

var ItemsService = newItemService()

func newItemService() itemService {
	return &itemServiceImpl{}
}

func (s *itemServiceImpl) Get(id int64) (*items.Item, *errors.RestErr) {
	return nil, errors.NewInternalError("implement me")
}

func (s *itemServiceImpl) Create(item items.Item) (*items.Item, *errors.RestErr) {
	err := item.Save()
	if err != nil {
		return nil, errors.NewInternalError(err.Error())
	}
	return &item, nil
}
