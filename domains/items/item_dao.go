package items

import "bookstore_items-api/clients/elasticsearch"

func (i *Item) Save() error {
	id, err := elasticsearch.Client.Index("item", i)
	if err == nil {
		i.Id = id
	}
	return err
}
