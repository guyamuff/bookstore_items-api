package items

type Item struct {
	Id                string      `json:"id"`
	Seller            int64       `json:"seller"`
	Title             string      `json:"title"`
	Description       Description `json:"description"`
	Pictures          []Picture   `json:"pictures"`
	Video             string      `json:"video"`
	Price             float64     `json:"price"`
	QuantityAvailable int64       `json:"quantity_available"`
	QuantitySold      int64       `json:"quantity_sold"`
	Status            string      `json:"status"`
}

type Description struct {
	Text string `json:"text"`
	Html string `json:"html"`
}

type Picture struct {
	Id  int64  `json:"id"`
	Url string `json:"url"`
}
